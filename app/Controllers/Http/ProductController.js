'use strict'

const Product = use("App/Models/Product");


class ProductController {

    async index({ response }) {
        const product = await Product.query()
            .with('category')
            .with('supplier')
            .fetch();
        if (product) {
            return response.status(201).json(product);
        } else {
            return response.status(400).json({ msg: "There's been a problem." });
        }
    }

    async store({ request, response }) {
        let product = new Product();
        let object = request.all();
        product.productName = object.productName;
        product.supplierID = object.supplierID;
        product.categoryID = object.categoryID;
        product.quantityPerUnit = object.quantityPerUnit;
        product.unitPrice = object.unitPrice;
        product.unitsInStock = object.unitsInStock;
        product.unitsOnOrder = object.unitsOnOrder;
        product.reorderLevel = object.reorderLevel;

        try {
            let data = await product.save();
            if (data) {
                return response.status(201).json(product);
            }
        } catch (error) {
            return response
                .status(400)
                .json({ msg: "Wrong Information", error: error });
        }
    }

    async show({ params, response }) {
        const product = await Product.find(params.id);
        try {
            return response.status(201).json(product);
        } catch (error) {
            return response
                .status(400)
                .json({ msg: "Wrong Information", error: error });
        }
    }

    async update({ params, request, response }) {
        const product = await Product.find(params.id);
        let object = request.all();
        product.productName = object.productName;
        product.supplierID = object.supplierID;
        product.categoryID = object.categoryID;
        product.quantityPerUnit = object.quantityPerUnit;
        product.unitPrice = object.unitPrice;
        product.unitInStock = object.unitInStock;
        product.unitOnOrder = object.unitOnOrder;
        product.reorderLevel = object.reorderLevel;

        try {
            let data = await product.save();
            if (data) {
                return response.status(201).json(product);
            }
        } catch (error) {
            return response
                .status(400)
                .json({ msg: "Wrong Information", error: error });
        }
    }

    async destroy({ params, response }) {
        const product = await Product.find(params.id);
        product.status = 0;

        try {
            let data = await product.save();
            if (data) {
                return response.status(201).json({ mgs: "Removed." });
            }
        } catch (error) {
            return response
                .status(400)
                .json({ msg: "Don´t Romoved.", error: error });
        }
    }

}

module.exports = ProductController