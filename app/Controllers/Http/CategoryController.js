'use strict'

const Category = use("App/Models/Category");
const Database = use("Database");


class CategoryController {
    async index({ response }) {
        const category = await Database.table("categories").where("status", 1);
        if (category) {
            return response.status(201).json(category);
        } else {
            return response.status(400).json({ msg: "There's been a problem." });
        }
    }

    async store({ request, response }) {
        let category = new Category();
        let object = request.all();
        category.categoryName = object.categoryName;
        category.description = object.description;

        try {
            let data = await category.save();
            if (data) {
                return response.status(201).json(category);
            }
        } catch (error) {
            return response
                .status(400)
                .json({ msg: "Wrong Information", error: error });
        }
    }

    async show({ params, response }) {
        const category = await Category.find(params.id);
        try {
            return response.status(201).json(category);
        } catch (error) {
            return response
                .status(400)
                .json({ msg: "Wrong Information", error: error });
        }
    }

    async update({ params, request, response }) {
        const category = await Category.find(params.id);
        let object = request.all();
        category.categoryName = object.categoryName;
        category.description = object.description;

        try {
            let data = await category.save();
            if (data) {
                return response.status(201).json(category);
            }
        } catch (error) {
            return response
                .status(400)
                .json({ msg: "Wrong Information", error: error });
        }

    }

    async destroy({ params, response }) {
        const category = await Category.find(params.id);
        category.status = 0;

        try {
            let data = await category.save();
            if (data) {
                return response.status(201).json({ mgs: "Removed." });
            }
        } catch (error) {
            return response
                .status(400)
                .json({ msg: "Don´t Removed", error: error });
        }
    }
}

module.exports = CategoryController