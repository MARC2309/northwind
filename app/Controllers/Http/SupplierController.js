'use strict'

const Supplier = use("App/Models/Supplier");

class SupplierController {

    async index({ response }) {
        const supplier = await Supplier.query()
            .where("status", 1)
            .fetch();
        if (supplier) {
            return response.status(201).json(supplier);
        } else {
            return response.status(400).json({ msg: "There's been a problem." });
        }

    }

    async store({ request, response }) {
        let supplier = new Supplier();
        let object = request.all();
        supplier.companyName = object.companyName;
        supplier.contactName = object.contactName;
        supplier.contactTitle = object.contactTitle;
        supplier.address = object.address;
        supplier.city = object.city;
        supplier.region = object.region;
        supplier.postalCode = object.postalCode;
        supplier.country = object.country;
        supplier.phone = object.phone;
        supplier.fax = object.fax;

        try {
            let data = await supplier.save();
            if (data) {
                return response.status(201).json(supplier);
            }
        } catch (error) {
            return response
                .status(400)
                .json({ msg: "Wrong Information", error: error });
        }
    }

    async show({ params, response }) {
        const supplier = await Supplier.find(params.id);
        try {
            return response.status(201).json(supplier);
        } catch (error) {
            return response
                .status(400)
                .json({ msg: "Wrong Information", error: error });
        }
    }

    async update({ params, request, response }) {
        const supplier = await Supplier.find(params.id);
        let object = request.all();
        supplier.companyName = object.companyName;
        supplier.contactName = object.contactName;
        supplier.contacTitle = object.contacTitle;
        supplier.address = object.Address;
        supplier.city = object.city;
        supplier.region = object.region;
        supplier.postalCode = object.postalCode;
        supplier.country = object.country;
        supplier.phone = object.phone;
        supplier.fax = object.fax;

        try {
            let data = await supplier.save();
            if (data) {
                return response.status(201).json(supplier);
            }
        } catch (error) {
            return response
                .status(400)
                .json({ msg: "Wrong Information", error: error });
        }
    }

    async destroy({ params, response }) {
        const supplier = await Supplier.find(params.id);
        supplier.status = 0;

        try {
            let data = await supplier.save();
            if (data) {
                return response.status(201).json({ mgs: "Removed." });
            }
        } catch (error) {
            return response
                .status(400)
                .json({ msg: "Don´t Romoved.", error: error });
        }
    }
}

module.exports = SupplierController