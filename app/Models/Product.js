'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Product extends Model {
    category() {
        return this.belongsTo('App/Models/Category', 'categoryID');
    }
    supplier() {
        return this.belongsTo('App/Models/Supplier', 'supplierID');
    }
}

module.exports = Product