'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

// Route.get('/', () => {
//     return { greeting: 'Hello world in JSON' }
// })

//Supplier
Route.get("/suppliers", "SupplierController.index");
Route.post("/supplier", "SupplierController.store");
Route.get("/supplier/:id", "SupplierController.show");
Route.put("/supplier/:id", "SupplierController.update");
Route.delete("/supplier/:id", "SupplierController.destroy");

//Products
Route.get("/products", "ProductController.index");
Route.post("/product", "ProductController.store");
Route.get("/product/:id", "ProductController.show");
Route.put("/product/:id", "ProductController.update");
Route.delete("/product/:id", "ProductController.destroy");

//Category
Route.get("/categories", "CategoryController.index");
Route.post("/category", "CategoryController.store");
Route.get("/category/:id", "CategoryController.show");
Route.put("/category/:id", "CategoryController.update");
Route.delete("/category/:id", "CategoryController.destroy");