'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SupplierSchema extends Schema {
    up() {
        this.create('suppliers', (table) => {
            table.bigIncrements();
            table.string('companyName', 80).notNullable();
            table.string('contactName', 80).notNullable();
            table.string('contactTitle', 80).notNullable();
            table.string('address', 80).notNullable();
            table.string('city', 80).notNullable();
            table.string('region', 80).notNullable();
            table.integer('postalCode').notNullable();
            table.string('country', 80).notNullable();
            table.string('phone', 20).notNullable();
            table.string('fax', 20).notNullable();
            table
                .integer("status")
                .defaultTo(1)
                .notNullable();
            table.timestamps();
        })
    }

    down() {
        this.drop('suppliers')
    }
}

module.exports = SupplierSchema