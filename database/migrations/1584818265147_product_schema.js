'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductSchema extends Schema {
    up() {
        this.create('products', (table) => {
            table.bigIncrements();
            table.string("productName", 50).notNullable();
            table
                .bigInteger("supplierID")
                .unsigned()
                .notNullable()
                .references("id")
                .inTable("suppliers");
            table
                .bigInteger("categoryID")
                .unsigned()
                .notNullable()
                .references("id")
                .inTable("categories");
            table.string("quantityPerUnit", 50).notNullable();
            table.float("unitPrice").notNullable();
            table.integer("unitsInStock").notNullable();
            table.integer("unitsOnOrder").notNullable();
            table.integer("reorderLevel").notNullable();
            table
                .integer("status")
                .defaultTo(1)
                .notNullable();
            table.timestamps();

        })
    }

    down() {
        this.drop('products')
    }
}

module.exports = ProductSchema