'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class CategorySchema extends Schema {
    up() {
        this.create('categories', (table) => {
            table.bigIncrements();
            table.string("categoryName", 50).notNullable();
            table.string("description", 50).notNullable();
            table
                .integer("status")
                .defaultTo(1)
                .notNullable();
            table.timestamps();
        })
    }

    down() {
        this.drop('categories');
    }
}

module.exports = CategorySchema